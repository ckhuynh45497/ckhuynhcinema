import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import HomePage from './containers/Home/HomePage';
import MovieDetail from './containers/Home/MovieDetail';
import TicketBooking from './containers/Home/TicketBooking';
import HeaderMovie from './components/Navbar';
import Auth  from './containers/Admin/Auth';
import  Dashboard  from './containers/Admin/Dashboard';
import AddUser from './containers/Admin/AddUser';




function App() {
  return (
    <div className="App">      
      <BrowserRouter>
      <HeaderMovie />
      <Switch>
        <Route path="/detail-movie/:movieID" component={MovieDetail} />
        <Route path="/booking/:bookingID" render={routeProps =>{
          if(localStorage.getItem("credential")){
            return<TicketBooking {...routeProps} />
          }else{
            alert("vui lòng đăng nhập");
            return<Redirect to ="/" />
          }
        }} />
        <Route path="/auth" component = {Auth} />
        <Route path="/dasboard" component ={Dashboard}/>
        <Route path="/add-user" component = {AddUser} />
        <Route exact path="/" component={HomePage} />
        
      </Switch>
      
      </BrowserRouter>
     {/* <HomePage/>
     <MovieDetail/>
    <TicketBooking /> */}
    </div>
  );
}

export default App;
