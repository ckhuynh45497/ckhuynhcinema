import React from "react";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';



export default function NavBarBS() {
  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">CyberCinema</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto text-white">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#link">About</Nav.Link>
            <Nav.Link href="#detail">Detail</Nav.Link>
            <Nav.Link>Login</Nav.Link>            
          </Nav>         
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}
