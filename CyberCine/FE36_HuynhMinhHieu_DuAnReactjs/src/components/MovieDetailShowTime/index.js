import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import MovieDetailShowtimeItem from "../MovieDetailShowTimeItem";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: "100%",
    padding: "100px 15%",
    textAlign: "left",
    
  },
  tabs: {    
    borderRight: `1px solid ${theme.palette.divider}`,
    
  },
}));

function MovieDetailShowTime(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  function _renderCinemaGroup(list) {
    if (list) {
      return list.map((item, index) => {
        return (
          <Tab
            label={
              <img
                src={item.logo}
                alt="logo"
                style={{ height: "75px", width: "75px" }}
              />
            }
            {...a11yProps(index)}
          />
        );
      });
    }
  }
  function _renderShowTime(list) {
    if (list) {
      return list.map((item, index) => {
        return (
          <TabPanel value={value} index={index} style={{ width: "100%",overflow:"auto",height:"650px"}}>
            <MovieDetailShowtimeItem cinemaList={item.cumRapChieu} />            
          </TabPanel>
        );
      });
    }
  }

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        {_renderCinemaGroup(props.movieDetail.heThongRapChieu)}
      </Tabs>
      {_renderShowTime(props.movieDetail.heThongRapChieu)}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    movieDetail: state.movieDetailReducer.movieInfo,
  };
};

export default connect(mapStateToProps)(MovieDetailShowTime);
