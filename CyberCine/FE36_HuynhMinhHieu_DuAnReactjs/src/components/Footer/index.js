import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "25px 0",
    backgroundColor: "#343a40!important",
  },
  name: {
    fontSize: "25px",
    color: "orange",
    fontWeight: "bold",
  },
  logo: {
    height: "40px",
    width: "40px",
    margin: "0 5px",
  },
  partner: {
    textAlign: "left",
    color: "white",
  },
}));

function Footer() {
  const classes = useStyles();
  return (
    <div class={classes.root}>
      <Grid container>
        <Grid item xs={6} className={classes.name}>
          <p>CyperCinema</p>
        </Grid>
        <Grid item xs={6} className={classes.partner}>
          <p >PARTNER</p>
          <img
            src="http://movie0706.cybersoft.edu.vn/hinhanh/bhd-star-cineplex.png"
            alt="logo"
            className={classes.logo}
          />
          <img
            src="http://movie0706.cybersoft.edu.vn/hinhanh/cgv.png"
            alt="logo"
            className={classes.logo}
          />
          <img
            src="http://movie0706.cybersoft.edu.vn/hinhanh/cinestar.png"
            alt="logo"
            className={classes.logo}
          />
          <img
            src="http://movie0706.cybersoft.edu.vn/hinhanh/galaxy-cinema.png"
            alt="logo"
            className={classes.logo}
          />
          <img
            src="http://movie0706.cybersoft.edu.vn/hinhanh/lotte-cinema.png"
            alt="logo"
            className={classes.logo}
          />
          <img
            src="http://movie0706.cybersoft.edu.vn/hinhanh/megags.png"
            alt="logo"
            className={classes.logo}
          />
        </Grid>
      </Grid>
    </div>
  );
}

export default Footer;
