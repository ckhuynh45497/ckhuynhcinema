import React, { useEffect } from "react";
import Input from "@material-ui/core/Input";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";

import Grid from "@material-ui/core/Grid";

import FormHelperText from "@material-ui/core/FormHelperText";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      width: "95%",
      margin: "0 auto",
    },
    "& label": {
      color: "orange",
      fontSize: "14px",
    },
    "& label.Mui-focused": {
      color: "orange",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "orange",
    },
    "& .MuiInput-underline:before": {
      borderBottomColor: "orange",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottomColor: "#b2ff4f",
    },
    flexGrow: 1,
    margin: ".5rem",
  },
  textField: {
    margin: "10%",
  },
  button: {
    backgroundColor: "orange",
    margin: "10%",
    color: "White",
    width: "80%",
    "&:hover": {
      backgroundColor: "orange",
    },
    "&:focus": {
      outline: "none",
    },
  },
}));

function SignUp(props) {
  const classes = useStyles();

  function handleSubmit() {
    if (props.dirty && props.isValid) {
      axios({
        method: "POST",
        url: "http://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
        data: {
            taiKhoan:props.values.taiKhoan,
            matKhau:props.values.matKhau,
            email:props.values.email,
            soDt:props.values.soDt,
            maNhom:"GP13",
            maLoaiNguoiDung:"KhachHang",
            hoTen:`${props.values.ho} ${props.values.ten}`
        }
      })
        .then((res) => {
          console.log(res.data);
          localStorage.setItem("credential", JSON.stringify(res.data));
          alert("Đăng ký thành công vui lòng đăng nhập");       
        })
        .catch((err) => {
          console.log(err.response.data);
        });
    }
    
  }
  useEffect(() => {
    //useEffect = life cycle didupdate
    console.log("useEffect Didupdate");
  }, [props.values]);
  return (
    <Form className={classes.root}>
      <Grid container justify="space-between" alignContent="center" spacing={2}>
        <Grid item xs={12} sm={6}>
          <FormControl
            fullWidth
            size="medium"
            margin="normal"
            error={props.touched.ho && !!props.errors.ho}
          >
            <InputLabel>Họ</InputLabel>
            <Field
              name="ho"
              render={({ field }) => (
                <Input className="pl-2" fullWidth {...field} />
              )}
            />
            {props.touched.ho && (
              <FormHelperText>{props.errors.ho}</FormHelperText>
            )}
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormControl
            fullWidth
            size="medium"
            margin="normal"
            error={props.touched.ten && !!props.errors.ten}
          >
            <InputLabel>Tên</InputLabel>
            <Field
              name="ten"
              render={({ field }) => (
                <Input className="pl-2" fullWidth {...field} />
              )}
            />
            {props.touched.ten && (
              <FormHelperText>{props.errors.ten}</FormHelperText>
            )}
          </FormControl>
        </Grid>
        <Grid item sm={12}>
          <FormControl
            fullWidth
            size="medium"
            margin="normal"
            error={props.touched.taiKhoan && !!props.errors.taiKhoan}
          >
            <InputLabel>Tài Khoản</InputLabel>
            <Field
              name="taiKhoan"
              render={({ field }) => (
                <Input className="pl-2" fullWidth {...field} />
              )}
            />
            {props.touched.taiKhoan && (
              <FormHelperText>{props.errors.taiKhoan}</FormHelperText>
            )}
          </FormControl>
        </Grid>
        <Grid item sm={12}>
          <FormControl
            fullWidth
            margin="normal"
            error={props.touched.matKhau && !!props.errors.matKhau}
          >
            <InputLabel>Mật Khẩu</InputLabel>
            <Field
              name="matKhau"
              render={({ field }) => (
                <Input className="pl-2" fullWidth type="password" {...field} />
              )}
            />
            {props.touched.matKhau && (
              <FormHelperText>{props.errors.matKhau}</FormHelperText>
            )}
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormControl
            fullWidth
            size="medium"
            margin="normal"
            error={props.touched.email && !!props.errors.email}
          >
            <InputLabel>Email</InputLabel>
            <Field
              name="email"
              render={({ field }) => (
                <Input className="pl-2" fullWidth {...field} />
              )}
            />
            {props.touched.email && (
              <FormHelperText>{props.errors.email}</FormHelperText>
            )}
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormControl
            fullWidth
            size="medium"
            margin="normal"
            error={props.touched.soDt && !!props.errors.soDt}
          >
            <InputLabel>Số Điện Thoại</InputLabel>
            <Field
              name="soDt"
              render={({ field }) => (
                <Input className="pl-2" fullWidth {...field} />
              )}
            />
            {props.touched.soDt && (
              <FormHelperText>{props.errors.soDt}</FormHelperText>
            )}
          </FormControl>
        </Grid>
        <FormControl fullWidth margin="normal">
          <Button
            className={classes.button}
            variant="extendedFab"
            color="primary"
            type="submit"
            onClick={handleSubmit}
          >
            Đăng Ký
          </Button>
        </FormControl>
      </Grid>
    </Form>
  );
}

const FormikFormSignUp = withFormik({
  mapPropsToValues() {
    return {
      taiKhoan: "",
      matKhau: "",
      ho:"",
      ten:"",
      email:"",
      soDt:"",
    };
  },
  validationSchema: Yup.object().shape({
    taiKhoan: Yup.string()
      .required("Vui lòng nhập Tài Khoản")
      .min(6, " Vui lòng nhập hơn 6 ký tự")
      .max(15, " Vui lòng nhập không quá 15 ký tự"),
    matKhau: Yup.string()
      .required("Vui lòng nhập Mật Khẩu")
      .min(6, " Vui lòng nhập hơn 6 ký tự")
      .max(15, " Vui lòng nhập không quá 15 ký tự"),
    ho :Yup.string().required("Vui lòng nhập Họ"),
    ten :Yup.string().required("Vui lòng nhập Tên"),
    soDt :Yup.string().required("Vui lòng nhập Số Điện Thoại"),
    email: Yup.string()
        .required("Vui lòng nhập Email")
        .email("Email không hợp lệ")
  }),
})(SignUp);

export default FormikFormSignUp;
