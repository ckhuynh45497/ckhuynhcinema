import React, { Fragment } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "90%",
    margin: "20px auto",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
  },
  btn: {
    backgroundColor: "#fb4226 !important",
    width: "90%",
    margin: "0 auto",
    outline: "none !important",
  },
}));

function TicketBookingInfo(props) {
  const classes = useStyles();
  function _handleBooking() {
   if(props.seatNumber===0){
    alert(" bạn chưa chọn ghế");
   }else{
    Axios({
      method: "POST",
      url: "http://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/DatVe",
      data: {
        maLichChieu: props.bookingId,
        danhSachVe: props.bookingList,
        taiKhoanNguoiDung: JSON.parse(localStorage.getItem("credential"))
          .taiKhoan,
      },
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("credential")).accessToken
        }`,
      },
    })
      .then((res) => {
        console.log(res.data);
        alert(res.data);
        window.location.replace("http://localhost:3000/")        
      })
      .catch((err) => {
        console.log(err.response.data);
      });
     
   }
  }
  if (props.movieInfo) {
    return (
      <Fragment>
        <div className={classes.root}>
          <Paper className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={4}>
                <img
                  src={props.movieInfo.hinhAnh}
                  style={{ width: "100px", height: "150px" }}
                  alt="movieimg"
                />
              </Grid>
              <Grid item xs={8}>
                <p>{props.movieInfo.tenPhim}</p>
                <p>Ngày Chiếu: {props.movieInfo.ngayChieu}</p>
                <p>Giờ Chiếu: {props.movieInfo.gioChieu}</p>
              </Grid>
            </Grid>
          </Paper>
        </div>
        <div className={classes.root}>
          <Paper className={classes.paper}>
            <p>{props.movieInfo.tenCumRap}</p>
            <p>{props.movieInfo.diaChi} </p>
            <p>
              {props.movieInfo.tenRap}{" "}
              <span> Số Ghế Đã Đặt: {props.seatNumber} </span>
            </p>
            <p>Tổng cộng: {props.total}</p>
          </Paper>
        </div>
        <Button className={classes.btn} onClick={_handleBooking}>dat ve</Button>
      </Fragment>
    );
  }
  return <div>boking info</div>;
}

const mapStateToProps = (state) => {
  return {
    seatNumber: state.ticketBookingReducer.seatNumber,
    total: state.ticketBookingReducer.total,
    bookingList: state.ticketBookingReducer.bookingList,
  };
};

export default connect(mapStateToProps)(TicketBookingInfo);
