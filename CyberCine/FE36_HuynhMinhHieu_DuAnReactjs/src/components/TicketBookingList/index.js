import React from 'react'
import TicketBookingListItem from '../TicketBookingListItem'

function TicketBookingList(props) {

    function _renderSeat (){
       if(props.seatList){
        return props.seatList.map((item,index)=>{
            return <TicketBookingListItem seat={item} key={index} />
        })
       }
    }

    return (
        <div>
            <img src="https://tix.vn/app/assets/img/icons/screen.png" alt="screen" />
            <div>
            {_renderSeat()}
            </div>
        </div>
    )
}

export default TicketBookingList
