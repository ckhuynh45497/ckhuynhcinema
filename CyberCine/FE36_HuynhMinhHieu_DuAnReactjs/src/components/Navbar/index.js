import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import LogInTabs from "../Tabs";
import { NavLink } from "react-router-dom";
import "./style.css";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "30%",
    marginLeft: "35%",
  },
  logo: {
    color: "orange",
    fontSize: "30px",
    fontWeight: "bold",
    textDecoration: "none !important",
    "& :hover": { textDecoration: "none !important" },
  },
  navLink:{
    color:"rgba(255,255,255,.5)",
    padding:".5rem 1rem",
    cursor:"pointer",
    "&:hover":{
      color:"orange",
      textDecoration:"none",
    }
  }
}));

function HeaderMovie() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const credential = JSON.parse(localStorage.getItem("credential"));

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const _handleSignOut = () => {
    localStorage.removeItem("credential");
    window.location.reload();
  };
  return (
    <>
      <Navbar
        bg="dark"
        variant="dark"
        expand="xl"
        // className="position-fixed"
        style={{ width: "100%", zIndex: "100", top: "0", position: "sticky" }}
      >
        <div className="container">
          <NavLink to="/" className={classes.logo}>
            CyberCinema
          </NavLink>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className={`${"mx-auto"}`}>
              <NavLink to="/" className={classes.navLink}>TRANG CHỦ</NavLink>
              <Nav.Link>LỊCH CHIẾU</Nav.Link>
              <Nav.Link>CỤM RẠP</Nav.Link>
              <Nav.Link>TINH TỨC</Nav.Link>
              <Nav.Link>LIÊN HỆ</Nav.Link>
              {credential ? (
                <>
                  {" "}
                  <li className="nav-item active">
                    <span className="nav-link" style={{color:"rgba(255,255,255,.5)"}}>
                      Xin Chào {credential.hoTen}{" "}
                    </span>
                  </li>
                  <Nav.Link onClick={_handleSignOut}>Đăng Xuất</Nav.Link>{" "}
                </>
              ) : (
                <Nav.Link onClick={handleOpen}>ĐĂNG NHẬP</Nav.Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <LogInTabs />
        </Fade>
      </Modal>
    </>
  );
}

export default HeaderMovie;
