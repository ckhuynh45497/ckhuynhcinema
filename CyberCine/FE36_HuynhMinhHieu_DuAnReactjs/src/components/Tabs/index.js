import React from "react";
import PropTypes from "prop-types";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import SignIn from "../SignIn";
import SignUp from "../SignUp";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
const AntTabs = withStyles({
  root: {
    borderBottom: "red",
    backgroundColor: "white",
    color: "black",
  },
  indicator: {
    backgroundColor: "orange",
    padding: 1,
  },
})(Tabs);
const AntTab = withStyles((theme) => ({
  root: {
    textTransform: "none",
    minWidth: 72,
    fontWeight: 500,
    fontSize: "18px",
    marginRight: theme.spacing(4),
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:hover": {
      color: "#b2ff4f",
      opacity: 1,
    },
    "&$selected": {
      color: "orange",
      fontWeight: 600,
      fontSize: "25px",
    },
    "&:focus": {
      color: "orange",
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: "white",
    "& button:focus": {
      outline: "none",
    },
  },
  tabContent: {
    width: "90%",
    margin: "0 auto",
    alignItems: "center",
    justifyContent: "cener",
  },
}));

function LogInTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <AntTabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
          centered
        >
          <AntTab label="ĐĂNG NHẬP" {...a11yProps(0)} />
          <AntTab label="ĐĂNG KÝ" {...a11yProps(1)} />
        </AntTabs>
      </AppBar>
      <TabPanel value={value} index={0} className={classes.tabContent}>
        <SignIn />
      </TabPanel>
      <TabPanel value={value} index={1} className={classes.tabContent}>
        <SignUp />
      </TabPanel>
    </div>
  );
}

export default LogInTabs;
