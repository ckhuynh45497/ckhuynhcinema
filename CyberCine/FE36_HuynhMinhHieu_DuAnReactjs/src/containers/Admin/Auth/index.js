import React, { useState } from "react";
import { connect } from "react-redux";
import {actPostAdminLoginAPI} from "./modules/action"

function Auth (props) {
  const [state, setState] = useState({ taiKhoan: "", matKhau: "" });

  const handleOnchange = (e) => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.postLogin(state, props.history);
  };

  const renderNotify = () => {
    const { infoUserError } = props;
    if (infoUserError) {
      return (
        <div className="alert alert-danger">{infoUserError.response.data}</div>
      );
    }
    return null;
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-6 mx-auto">
          <form onSubmit={handleSubmit}>
            <h1>Login Admin</h1>

            {renderNotify()}

            <div className="form-group">
              <label>Username</label>
              <input
                className="form-control"
                type="text"
                name="taiKhoan"
                onChange={handleOnchange}
                value={state.taiKhoan}
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                className="form-control"
                type="text"
                name="matKhau"
                onChange={handleOnchange}
                value={state.matKhau}
              />
            </div>
            <button type="submit" className="btn btn-success">
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.authReducer.loadingInfoUSer,
    infoUserError: state.authReducer.infoUserError,
  };
};

const mapDispatchToProps = (dispatch) => {
  return{
    postLogin: (user,history) =>{
      dispatch(actPostAdminLoginAPI(user,history))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
