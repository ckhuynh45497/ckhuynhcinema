import React, { useEffect } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/core/styles';
import { actFetchMovieDetailAPI } from "./module/action";
import MovieDetailShowTime from "../../../components/MovieDetailShowTime";
import Loading from "../../../components/Loading";
import Footer from "../../../components/Footer";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding:"100px 15%"
  },
  inFo:{
    textAlign:"left",
    "& h2":{
      margin:"10px 0"
    }
  }
}));

function MovieDetail(props) {
  const classes = useStyles();
  useEffect(() => {
    props.fetchMovieDetail(props.match.params.movieID);
  }, []);
  if(props.loading) return <Loading />
  return (
    <div>
      <Grid container className={classes.root} justify="flex-start" alignItems="center" spacing={2}>
        <Grid item xs={3}>
          <img src={props.movieDetail.hinhAnh} alt="movie" style={{width:"214px",height:"339px"}}/>
        </Grid>
        <Grid item xs={9} className={classes.inFo}>
          <h2>{props.movieDetail.tenPhim}</h2>
          <p>{props.movieDetail.moTa}</p>
        </Grid>
      </Grid>
      <h2>Lich Chieu</h2>
      <MovieDetailShowTime/>
      <Footer />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    loading: state.movieDetailReducer.loading,
    movieDetail: state.movieDetailReducer.movieInfo,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchMovieDetail: (id) => {
      dispatch(actFetchMovieDetailAPI(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetail);
