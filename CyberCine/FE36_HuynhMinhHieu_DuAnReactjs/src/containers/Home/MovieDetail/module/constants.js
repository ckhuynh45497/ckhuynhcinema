export const FETCH_MOVIE_DETAIL_REQUEST =
  "MovieDetail/MovieDetailReducer/FETCH_MOVIE_DETAIL_REQUEST";
export const FETCH_MOVIE_DETAIL_SUCCESS =
  "MovieDetail/MovieDetailReducer/FETCH_MOVIE_DETAIL_SUCCESS";
export const FETCH_MOVIE_DETAIL_FAILED =
  "MovieDetail/MovieDetailReducer/FETCH_MOVIE_DETAIL_FAILED";
