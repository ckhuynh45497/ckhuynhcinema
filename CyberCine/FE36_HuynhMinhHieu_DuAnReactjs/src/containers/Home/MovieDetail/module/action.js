import Axios from "axios";
import {
  FETCH_MOVIE_DETAIL_REQUEST,
  FETCH_MOVIE_DETAIL_SUCCESS,
  FETCH_MOVIE_DETAIL_FAILED,
} from "./constants";
import { ADD_BOOKING_SEAT, REMOVE_BOOKING_SEAT } from "../../TicketBooking/module/constants";
export const actFetchMovieDetailAPI = (id) => {
  return (dispatch) => {
    dispatch(actFetchMovieDetailRequest(id));
    Axios({
      method: "GET",
      url:
        `http://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`,
    })
      .then((res) => {
        dispatch(actFetchMovieDetailSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actFetchMovieDetailFailed(err));
      });
  };
};
export const actFetchMovieDetailRequest = () => {
  return {
    type: FETCH_MOVIE_DETAIL_REQUEST,
  };
};
export const actFetchMovieDetailSuccess = (movieDetail) => {
  return {
    type: FETCH_MOVIE_DETAIL_SUCCESS,
    data: movieDetail,
  };
};
export const actFetchMovieDetailFailed = (err) => {
  return {
    type: FETCH_MOVIE_DETAIL_FAILED,
    err,
  };
};
export const actAddBookingSeat = (maGhe,giaVe)=>{
  return{
    type:ADD_BOOKING_SEAT,
    data:{maGhe,giaVe}
  }
};
export const actRemoveBookingSeat = (maGhe,giaVe)=>{
  return{
    type:REMOVE_BOOKING_SEAT,
    data:{maGhe,giaVe}
  }
}
