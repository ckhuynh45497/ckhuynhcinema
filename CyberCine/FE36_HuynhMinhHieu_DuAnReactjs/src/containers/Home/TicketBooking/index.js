import React, { useEffect } from "react";
import { connect } from "react-redux";
import { actFetchTicketBookingAPI } from "./module/action";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import TicketBookingList from "../../../components/TicketBookingList";
import TicketBookingInfo from "../../../components/TicketBookingInfo";
import Loading from "../../../components/Loading";
import Footer from "../../../components/Footer";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "80%",
    margin: "50px auto",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

function TicketBooking(props) {
  const classes = useStyles();
  useEffect(() => {
    props.fetchTicketBoking(props.match.params.bookingID);
  }, []);
  if (props.loading) return <Loading />;
  return (
    <>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={8}>
            <TicketBookingList seatList={props.bookingInfo.danhSachGhe} />
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <TicketBookingInfo
                movieInfo={props.bookingInfo.thongTinPhim}
                bookingList={props.bookingList}
                bookingId={props.match.params.bookingID}
              />
            </Paper>
          </Grid>
          <Grid item xs={2}>
            <button
              style={{
                minWidth: "40px",
                maxWidth: "40px",
                height: "40px",
                margin: "10px",
                outline: "none",
                backgroundColor: "#3e515d",
                color: "white",
              }}
              disabled
            >
              {" "}
            </button>
            <p>Ghế trống</p>
          </Grid>
          <Grid item xs={2}>
            <button
              style={{
                minWidth: "40px",
                maxWidth: "40px",
                height: "40px",
                margin: "10px",
                outline: "none",
                backgroundColor: "yellow",
                color: "white",
              }}
              disabled
            >
              {" "}
            </button>
            <p>Ghế Vip</p>
          </Grid>
          <Grid item xs={2}>
            <button
              style={{
                minWidth: "40px",
                maxWidth: "40px",
                height: "40px",
                margin: "10px",
                outline: "none",
                backgroundColor: "green",
                color: "white",
              }}
              disabled
            >
              {" "}
            </button>
            <p>Ghế Đang Chọn</p>
          </Grid>
          <Grid item xs={2}>
            <button
              style={{
                minWidth: "40px",
                maxWidth: "40px",
                height: "40px",
                margin: "10px",
                outline: "none",
                backgroundColor: "red",
                color: "white",
              }}
              disabled
            >
              {" "}
            </button>
            <p>Ghế Đã Đặt</p>
          </Grid>
        </Grid>
      </div>
      <Footer />
    </>
  );
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetchTicketBoking: (id) => {
      dispatch(actFetchTicketBookingAPI(id));
    },
  };
};
const mapStateToProps = (state) => {
  return {
    bookingInfo: state.ticketBookingReducer.bookingInfo,
    bookingList: state.ticketBookingReducer.bookingList,
    loading: state.ticketBookingReducer.loading,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TicketBooking);
