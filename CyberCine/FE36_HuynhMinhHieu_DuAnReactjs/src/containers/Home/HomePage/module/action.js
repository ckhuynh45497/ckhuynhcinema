import Axios from "axios";
import {
  FETCH_LIST_MOVIE_REQUEST,
  FETCH_LIST_MOVIE_SUCCESS,
  FETCH_LIST_MOVIE_FAILED,
  FETCH_MAIN_CAROUSEL_REQUEST,
  FETCH_MAIN_CAROUSEL_SUCCESS,
  FETCH_MAIN_CAROUSEL_FAILED,
  FETCH_SHOWTIME_REQUEST,
  FETCH_SHOWTIMEL_SUCCESS,
  FETCH_SHOWTIME_FAILED,
  FETCH_CINEMA_GROUP_REQUEST,
  FETCH_CINEMA_GROUP_SUCCESS,
  FETCH_CINEMA_GROUP_FAILED,
} from "./constants";

export const actFetchListMovieAPI = () => {
  return (dispatch) => {
    dispatch(actFetchListMovieRequest());
    Axios({
      method: "GET",
      url:
        "http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP13",
    })
      .then((res) => {
        dispatch(actFetchListMovieSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actFetchListMovieFailed(err));
      });
  };
};

export const actFetchListMovieRequest = () => {
  return {
    type: FETCH_LIST_MOVIE_REQUEST,
  };
};
export const actFetchListMovieSuccess = (listMovie) => {
  return {
    type: FETCH_LIST_MOVIE_SUCCESS,
    data: listMovie,
  };
};
export const actFetchListMovieFailed = (err) => {
  return {
    type: FETCH_LIST_MOVIE_FAILED,
    err,
  };
};

export const actFetchMainCarouselAPI = () => {
  return (dispatch) => {
    dispatch(actFetchMainCarouselRequest());
    Axios({
      method: "GET",
      url: "https://5eeb377fb13d0a00164e531c.mockapi.io/trailer",
    })
      .then((res) => {
        dispatch(actFetchMainCarouselSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actFetchMainCarouselFailed(err));
      });
  };
};

export const actFetchMainCarouselRequest = () => {
  return {
    type: FETCH_MAIN_CAROUSEL_REQUEST,
  };
};
export const actFetchMainCarouselSuccess = (mainCarousel) => {
  return {
    type: FETCH_MAIN_CAROUSEL_SUCCESS,
    data: mainCarousel,
  };
};
export const actFetchMainCarouselFailed = (err) => {
  return {
    type: FETCH_MAIN_CAROUSEL_FAILED,
    err,
  };
};
export const actFetchShowTimeAPI = (id) => {
  return (dispatch) => {
    dispatch(actFetchShowTimeRequest());
    Axios({
      method: "GET",
      url: `http://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=${id}&maNhom=GP13`,
    })
      .then((res) => {
        dispatch(actFetchShowTimeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actFetchShowTimeFailed(err));
      });
  };
};

export const actFetchShowTimeRequest = () => {
  return {
    type: FETCH_SHOWTIME_REQUEST,
  };
};
export const actFetchShowTimeSuccess = (showTime) => {
  return {
    type: FETCH_SHOWTIMEL_SUCCESS,
    data: showTime,
  };
};
export const actFetchShowTimeFailed = (err) => {
  return {
    type: FETCH_SHOWTIME_FAILED,
    err,
  };
};
export const actFetchCinemaGroupAPI = ()=>{
  return (dispatch)=>{
    dispatch(actFetchCinemaGroupRequest());
    Axios({
      method:"GET",
      url:"http://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinHeThongRap"
    }).then((res)=>{
      dispatch(actFetchCinemaGroupSuccess(res.data));
    }).catch((err)=>{
      dispatch(actFetchCinemaGroupFailed(err));
    })
  }
}

export const actFetchCinemaGroupRequest =()=>{
  return{
    type: FETCH_CINEMA_GROUP_REQUEST
  }
}
export const actFetchCinemaGroupSuccess = (item)=>{
  return{
    type: FETCH_CINEMA_GROUP_SUCCESS,
    data:item
  }
}
export const actFetchCinemaGroupFailed = (err)=>{
  return{
    type: FETCH_CINEMA_GROUP_FAILED,
    err
  }
}