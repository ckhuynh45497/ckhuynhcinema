import {
  FETCH_LIST_MOVIE_REQUEST,
  FETCH_LIST_MOVIE_SUCCESS,
  FETCH_LIST_MOVIE_FAILED,
  FETCH_MAIN_CAROUSEL_REQUEST,
  FETCH_MAIN_CAROUSEL_SUCCESS,
  FETCH_MAIN_CAROUSEL_FAILED,
  FETCH_SHOWTIME_REQUEST,
  FETCH_SHOWTIMEL_SUCCESS,
  FETCH_SHOWTIME_FAILED,
  FETCH_CINEMA_GROUP_REQUEST,
  FETCH_CINEMA_GROUP_SUCCESS,
  FETCH_CINEMA_GROUP_FAILED,
} from "./constants";

let initialState = {
  mainCarousel: [],
  loadingListMovie: false,
  loadingMainCarousel: false,
  loadingShowtime: false,
  loadingCinemaGroup: false,
  listMovie: [],
  showTime: [],
  cinemaGroup: [],  
  mainCarouselError: null,
  listMovieError: null,
  showTimeError: null,
  cinemaGroupError: null,
};

const homePageReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LIST_MOVIE_REQUEST:
      state.listMovie = [];
      state.loadingListMovie = true;
      state.listMovieError = null;
      return { ...state };
    case FETCH_LIST_MOVIE_SUCCESS:
      state.listMovie = action.data;
      state.loadingListMovie = false;
      state.listMovieError = null;
      return { ...state };
    case FETCH_LIST_MOVIE_FAILED:
      state.listMovie = [];
      state.loadingListMovie = false;
      state.listMovieError = action.err;
      return { ...state };
    case FETCH_MAIN_CAROUSEL_REQUEST:
      state.mainCarousel = [];
      state.loadingMainCarousel = true;
      state.mainCarouselError = null;
      return { ...state };
    case FETCH_MAIN_CAROUSEL_SUCCESS:
      state.mainCarousel = action.data;
      state.loadingMainCarousel = false;
      state.mainCarouselError = null;
      return { ...state };
    case FETCH_MAIN_CAROUSEL_FAILED:
      state.mainCarousel = [];
      state.loadingMainCarousel = false;
      state.mainCarouselError = action.err;
      return { ...state };
    case FETCH_SHOWTIME_REQUEST:
      state.showTime = [];
      state.loadingShowtime = true;
      state.err = null;
      return { ...state };
    case FETCH_SHOWTIMEL_SUCCESS:
      state.showTime = action.data;
      state.loadingShowtime = false;
      state.showTimeError = null;
      return { ...state };
    case FETCH_SHOWTIME_FAILED:
      state.showTime = [];
      state.loadingShowtime = false;
      state.showTimeError = action.err;
      return { ...state };
    case FETCH_CINEMA_GROUP_REQUEST:
      state.cinemaGroup = [];
      state.loadingCinemaGroup = true;
      state.cinemaGroupError = null;
      return { ...state };
    case FETCH_CINEMA_GROUP_SUCCESS:
      state.cinemaGroup = action.data;
      state.loadingCinemaGroup = false;
      state.cinemaGroupError = null;
      return { ...state };
    case FETCH_CINEMA_GROUP_FAILED:
      state.cinemaGroup = [];
      state.loadingCinemaGroup = false;
      state.cinemaGroupError = action.err;
      return { ...state };
    default:
      return { ...state };
  }
};

export default homePageReducer;
