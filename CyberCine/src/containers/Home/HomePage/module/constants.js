export const FETCH_LIST_MOVIE_REQUEST =
  "HomePage/HomePageReducer/FETCH_LIST_MOVIE_REQUEST";
export const FETCH_LIST_MOVIE_SUCCESS =
  "Homepage/HomePageReducer/FETCH_LIST_MOVIE_SUCCESS";
export const FETCH_LIST_MOVIE_FAILED =
  "Homepage/HomePageReducer/FETCH_LIST_MOVIE_FAILED";
export const FETCH_MAIN_CAROUSEL_REQUEST =
  "HomePage/HomePageReducer/FETCH_MAIN_CAROUSEL_REQUEST";
export const FETCH_MAIN_CAROUSEL_SUCCESS =
  "Homepage/HomePageReducer/FETCH_MAIN_CAROUSEL_SUCCESS";
export const FETCH_MAIN_CAROUSEL_FAILED =
  "Homepage/HomePageReducer/FETCH_MAIN_CAROUSEL_FAILED";
export const FETCH_SHOWTIME_REQUEST =
  "HomePage/HomePageReducer/FETCH_SHOWTIME_REQUEST";
export const FETCH_SHOWTIMEL_SUCCESS =
  "Homepage/HomePageReducer/FETCH_SHOWTIME_SUCCESS";
export const FETCH_SHOWTIME_FAILED =
  "Homepage/HomePageReducer/FETCH_SHOWTIME_FAILED";
export const FETCH_CINEMA_GROUP_REQUEST =
  "HomePage/HomePageReducer/FETCH_CINEMA_GROUP_REQUEST";
export const FETCH_CINEMA_GROUP_SUCCESS =
  "Homepage/HomePageReducer/FETCH_CINEMA_GROUP_SUCCESS";
export const FETCH_CINEMA_GROUP_FAILED =
  "Homepage/HomePageReducer/FETCH_CINEMA_GROUP_FAILED";
