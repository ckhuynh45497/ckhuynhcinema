import React, { Fragment, useEffect } from "react";
import { connect } from "react-redux";

import { actFetchListMovieAPI, actFetchMainCarouselAPI, actFetchShowTimeAPI, actFetchCinemaGroupAPI } from "./module/action";
import HomePageMovieCarousel from "../../../components/HomePageMovieCarousel";
import HomePageMainCarousel from "../../../components/HomePageMainCarousel";
import HomePageShowTime from "../../../components/HomePageShowTime";
import Loading from "../../../components/Loading/index"
import Footer from "../../../components/Footer";

function HomePage(props) {
  useEffect(() => {
      props.fetchListMovie();
      props.fetchMainCarousel();
      props.fetchShowTime("BHDStar");
      props.fetchCinemaGroup();
      //eslint-disable-next-line react-hooks/exhaustive-deps      
  }, []);
  if(props.loadingCP && props.loadingLM && props.loadingS && props.loadingMC) return <Loading />
  return (
    <Fragment>      
      <HomePageMainCarousel/>
      <HomePageMovieCarousel/>      
      <HomePageShowTime/>
      <Footer />
    </Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    loadingLM: state.homePageReducer.loadingListMovie,
    loadingMC: state.homePageReducer.loadingMainCarousel,
    loadingS: state.homePageReducer.loadingShowtime,
    loadingCP: state.homePageReducer.loadingCinemaGroup,
    listMovie: state.homePageReducer.listMovie,
    mainCarousel: state.homePageReducer.mainCarousel,
    showTime: state.homePageReducer.showTime,
    cinemaGroup: state.homePageReducer.cinemaGroup,
   
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchListMovie: () => {
      dispatch(actFetchListMovieAPI());
    },
    fetchMainCarousel:()=>{
        dispatch(actFetchMainCarouselAPI());
    },
    fetchShowTime: (id)=>{
        dispatch(actFetchShowTimeAPI(id));
    },
    fetchCinemaGroup: ()=>{
        dispatch(actFetchCinemaGroupAPI());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
