import {
  FETCH_MOVIE_DETAIL_REQUEST,
  FETCH_MOVIE_DETAIL_SUCCESS,
  FETCH_MOVIE_DETAIL_FAILED,
} from "./constants";

let initialState = {
  movieInfo: {},
  movieDetailError: null,
  loading: false,
};

const movieDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MOVIE_DETAIL_REQUEST:
      state.movieInfo = {};
      state.loading = true;
      state.movieDetailError = null;
      return { ...state };
    case FETCH_MOVIE_DETAIL_SUCCESS:
      state.movieInfo = action.data;
      state.loading = false;
      state.movieDetailError = null;
      return { ...state };
    case FETCH_MOVIE_DETAIL_FAILED:
      state.movieInfo = {};
      state.loading = false;
      state.movieDetailError = action.err;
      return { ...state };
    default:
      return { ...state };
  }
};
export default movieDetailReducer;
