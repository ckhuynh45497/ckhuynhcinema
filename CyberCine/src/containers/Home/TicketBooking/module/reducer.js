import {
  FETCH_BOOKING_REQUEST,
  FETCH_BOOKING_SUCCESS,
  FETCH_BOOKING_FAILED,
  ADD_BOOKING_SEAT,
  REMOVE_BOOKING_SEAT,
} from "./constants";

let initialState = {
  bookingInfo: {},
  loading: false,
  bookingError: null,
  bookingList: [],
  seatNumber: 0,
  total: 0,
};

const ticketBookingReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_BOOKING_REQUEST:
      state.bookingInfo = {};
      state.loading = true;
      state.bookingError = null;
      return { ...state };
    case FETCH_BOOKING_SUCCESS:
      state.bookingInfo = action.data;
      state.loading = false;
      state.bookingError = null;
      return { ...state };
    case FETCH_BOOKING_FAILED:
      state.bookingInfo = {};
      state.loading = false;
      state.bookingError = action.err;
      return { ...state };
    case ADD_BOOKING_SEAT:
      state.bookingList.push(action.data);
      state.seatNumber = state.seatNumber + 1;
      state.total = state.total + parseInt(action.data.giaVe);
      return { ...state };
    case REMOVE_BOOKING_SEAT: {
      const index = state.bookingList.find(
        (item) => item.maGhe === action.data.maGhe
      );
      state.bookingList.splice(index, 1);
      state.seatNumber = state.seatNumber - 1;
      state.total = state.total - parseInt(action.data.giaVe);
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default ticketBookingReducer;
