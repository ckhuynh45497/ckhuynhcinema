import Axios from "axios";
import {
  FETCH_BOOKING_REQUEST,
  FETCH_BOOKING_SUCCESS,
  FETCH_BOOKING_FAILED,
} from "./constants";

export const actFetchTicketBookingAPI = (id) => {
  return (dispatch) => {
    dispatch(actFetchTicketBookingRequest());
    Axios({
      method: "GET",
      url:
        `http://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${id}`,
    })
      .then((res) => {
        dispatch(actFetchTicketBookingSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actFetchTicketBookingFailed(err));
      });
  };
};

export const actFetchTicketBookingRequest = () => {
  return {
    type: FETCH_BOOKING_REQUEST,
  };
};
export const actFetchTicketBookingSuccess = (bookingInfo) => {
  return {
    type: FETCH_BOOKING_SUCCESS,
    data: bookingInfo,
  };
};
export const actFetchTicketBookingFailed = (err) => {
  return {
    type: FETCH_BOOKING_FAILED,
    err,
  };
};
