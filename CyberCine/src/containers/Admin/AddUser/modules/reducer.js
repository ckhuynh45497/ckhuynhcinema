import {
    POST_ADD_USER_REQUEST,
    POST_ADD_USER_SUCCESS,
    POST_ADD_USER_FAILED
  } from "./constant";
  
  let initialState = {
    user: {},
    loading: false,
    error: null
  };
  
  const addUserReducer = (state = initialState, action) => {
    switch (action.type) {
      case POST_ADD_USER_REQUEST:
        state.user = {};
        state.loading = true;
        state.error = null;
        return { ...state };
  
      case POST_ADD_USER_SUCCESS:
        state.user = action.data;
        state.loading = false;
        state.error = null;
        return { ...state };
  
      case POST_ADD_USER_FAILED:
        state.user = {};
        state.loading = false;
        state.error = action.err;
        return { ...state };
  
      default:
        return { ...state };
    }
  };
  
  export default addUserReducer;
  