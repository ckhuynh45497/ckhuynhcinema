export const POST_ADD_USER_REQUEST =
  "AddUser/AddUserReducer/POST_ADD_USER_REQUEST";

export const POST_ADD_USER_SUCCESS =
  "AddUser/AddUserReducer/POST_ADD_USER_SUCCESS";

export const POST_ADD_USER_FAILED =
  "AddUser/AddUserReducer/POST_ADD_USER_FAILED";
