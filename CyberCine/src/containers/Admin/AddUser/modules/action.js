import Axios from "axios";
import { POST_ADD_USER_SUCCESS, POST_ADD_USER_FAILED,POST_ADD_USER_REQUEST} from "./constant";

export const actPostAddUserAPI = (user) => {
  let accessToken = "";
  if (localStorage.getItem("UserAdmin")) {
    accessToken = JSON.parse(localStorage.getItem("UserAdmin")).accessToken;
  }
  return (dispatch) => {
    dispatch(actPostAddUserRequest());
    Axios({
      method: "POST",
      url:
        "http://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThemNguoiDung",
      data: user,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        dispatch(actPostAddUserSuccess(res.data));
        console.log(res.data);
        alert("them tai khoan thanh cong");
      })
      .catch((err) => {
        dispatch(actPostAddUserFailed(err));
        alert(err.response.data)
      });
  };
};
const actPostAddUserRequest = () => {
  return {
    type: POST_ADD_USER_REQUEST,
  };
};
const actPostAddUserSuccess = (data) => {
  return {
    type: POST_ADD_USER_SUCCESS,
    data,
  };
};
const actPostAddUserFailed = (err) => {
  return {
    type: POST_ADD_USER_FAILED,
    err,
  };
};
