import React, { useState } from "react";
import { connect } from "react-redux";
import { actPostAddUserAPI } from "./modules/action";

function AddUSer(props) {
  const [state, setState] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    maNhom: "",
    maLoaiNguoiDung: "",
    hoTen: "",
  });

  const handleOnchange = (e) => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const handleSubmit = (e) => {
    
    props.addUser(state);
  };
  
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-6 mx-auto">
          <form>
            <h1>Add User</h1>

            <div className="form-group">
              <label>Username</label>
              <input
                className="form-control"
                type="text"
                name="taiKhoan"
                onChange={handleOnchange}
                value={state.taiKhoan}
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                className="form-control"
                type="text"
                name="matKhau"
                onChange={handleOnchange}
                value={state.matKhau}
              />
            </div>
            <div className="form-group">
              <label>email</label>
              <input
                className="form-control"
                type="text"
                name="email"
                onChange={handleOnchange}
                value={state.email}
              />
            </div>
            <div className="form-group">
                <label>Số điện thoại</label>
                <input
                  className="form-control"
                  type="text"
                  name="soDt"
                  onChange={handleOnchange}
                  value={state.soDt}
                />
                <div className="form-group">
                <label>Mã Nhóm</label>
                <input
                  className="form-control"
                  type="text"
                  name="maNhom"
                  onChange={handleOnchange}
                  value={state.maNhom}
                />
              </div>
              <div className="form-group">
                <label>Mã loại người dùng</label>
                <input
                  className="form-control"
                  type="text"
                  name="maLoaiNguoiDung"
                  onChange={handleOnchange}
                  value={state.maLoaiNguoiDung}
                />
              </div>
              </div>
              <div className="form-group">
                <label>Họ tên</label>
                <input
                  className="form-control"
                  type="text"
                  name="hoTen"
                  onChange={handleOnchange}
                  value={state.hoTen}
                />
              </div>
            <button type="button" className="btn btn-success" onClick={handleSubmit}>
              AddUser
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = dispatch => {
    return {
      addUser: user => {
        dispatch(actPostAddUserAPI(user));
      }
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(AddUSer);
