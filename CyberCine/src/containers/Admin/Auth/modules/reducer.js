const {
  POST_LOGIN_ADMIN_REQUEST,
  POST_LOGIN_ADMIN_SUCCESS,
  POST_LOGIN_ADMIN_FAILED,
} = require("./constant");

let initialState = {
  infoUser: {},
  loadingInfoUSer: false,
  infoUSerError: null,
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_LOGIN_ADMIN_REQUEST:
      state.infoUser = {};
      state.loadingInfoUSer = true;
      state.infoUSerError = null;
      return { ...state };
    case POST_LOGIN_ADMIN_SUCCESS:
      state.infoUser = action.data;
      state.loadingInfoUSer = false;
      state.infoUSerError = null;
      return { ...state };
    case POST_LOGIN_ADMIN_FAILED:
      state.infoUser = {};
      state.loadingInfoUSer = false;
      state.infoUSerError = action.err;
      return { ...state };
    default:
      return { ...state };
  }
};
export default AuthReducer;
