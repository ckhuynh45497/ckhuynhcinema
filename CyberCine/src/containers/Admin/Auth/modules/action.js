import Axios from "axios";
import {
  POST_LOGIN_ADMIN_REQUEST,
  POST_LOGIN_ADMIN_SUCCESS,
  POST_LOGIN_ADMIN_FAILED,
} from "./constant";

export const actPostAdminLoginAPI = (user, history) => {
  return (dispatch) => {
    dispatch(actPostAdminLoginRequest());
    Axios({
      method: "POST",
      url: "http://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      data: user,
    })
      .then((res) => {
        dispatch(actPostAdminLoginSuccess(res.data));
        localStorage.setItem("UserAdmin", JSON.stringify(res.data));
        history.push("/dasboard");
      })
      .catch((err) => {
        dispatch(actPostAdminLoginFailed(err));
      });
  };
};

const actPostAdminLoginRequest = () => {
  return {
    type: POST_LOGIN_ADMIN_REQUEST,
  };
};
const actPostAdminLoginSuccess = (data) => {
  return {
    type: POST_LOGIN_ADMIN_SUCCESS,
    data,
  };
};
const actPostAdminLoginFailed = (err) => {
  return {
    type: POST_LOGIN_ADMIN_FAILED,
    err,
  };
};
