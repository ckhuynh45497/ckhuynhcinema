import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Moment from "react-moment";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  button: {
    margin: "20px",
  },
}));

function HomePageShowTimeMovieItem(props) {
  const classes = useStyles();
  function _renderMovieShowTime(item) {
    return (
      <Fragment>
        <Button variant="outlined" color="primary" className={classes.button}>
          <NavLink
            to={`/booking/${item[0].maLichChieu}`}
          >
            <Moment format="HH:mm">{item[0].ngayChieuGioChieu}</Moment>
          </NavLink>
        </Button>
        <Button variant="outlined" color="primary" className={classes.button}>
        <NavLink
            to={`/booking/${item[1].maLichChieu}`}
          >
            <Moment format="HH:mm">{item[1].ngayChieuGioChieu}</Moment>
          </NavLink>
        </Button>
        <Button variant="outlined" color="primary" className={classes.button}>
        <NavLink
            to={`/booking/${item[2].maLichChieu}`}
          >
            <Moment format="HH:mm">{item[2].ngayChieuGioChieu}</Moment>
          </NavLink>
        </Button>
      </Fragment>
    );
  }
  function _renderMovie(list) {
    return list.map((item, index) => {
      return (
        <div className={classes.root}>
          <Grid container spacing={5} justify="center" alignItems="center">
            <Grid item>
              <img
                src={item.hinhAnh}
                alt="movieImg"
                style={{ width: "100px", height: "100px" }}
              />
            </Grid>
            <Grid item>
              <h3>{item.tenPhim}</h3>
              <p>Lịch Chiếu</p>
              {_renderMovieShowTime(item.lstLichChieuTheoPhim)}
            </Grid>
          </Grid>
        </div>
      );
    });
  }
  return <div>{_renderMovie(props.movies)}</div>;
}

export default HomePageShowTimeMovieItem;
