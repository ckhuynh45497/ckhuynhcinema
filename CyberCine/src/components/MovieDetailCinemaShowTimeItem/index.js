import React from "react";
import Grid from "@material-ui/core/Grid";
import Moment from "react-moment";

import { makeStyles } from "@material-ui/core/styles";
import {NavLink} from "react-router-dom"

const useStyles = makeStyles((theme) => ({
  button: {
    backgroundColor: "green !important",
    margin: "0 10px",
    color: "white",
    textTransform: "none",
    "& :hover": {
      color: "orange",
    },
  },
}));

function MovieDetailCinemaShowTimeItem(props) {
  const classes = useStyles();
  function _renderShowtime() {
    return props.showtimeList.map((item, index) => {
      return (
        <Grid item xs={6}>
          <span>Ngay Chiếu: </span>
          <Moment format="DD-MM-YYYY">{item.ngayChieuGioChieu}</Moment>
          <span> Giờ Chiếu: </span>
          <Moment format="HH:mm">{item.ngayChieuGioChieu}</Moment>
          <NavLink variant="contained" to={`/booking/${item.maLichChieu}`} className={`${classes.button} btn`}>
            Đặt vé
          </NavLink>
        </Grid>
      );
    });
  }

  return (
    <>
      <Grid
        container
        spacing={2}
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
      >
        {_renderShowtime()}
      </Grid>
    </>
  );
}

export default MovieDetailCinemaShowTimeItem;
