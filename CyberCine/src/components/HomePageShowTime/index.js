import React from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import HomePageShowTimeTabs from "../HomePageShowTimeTabs";
import { actFetchShowTimeAPI } from "../../containers/Home/HomePage/module/action";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));



function HomePageShowTime(props) {
    const classes = useStyles();
    
    const _renderCinemaGroupICon = (list)=>{
      return list.map((item,index)=>{
          return (
             <div onClick={()=>{props.fetchShowTime(item.maHeThongRap)}}>
                <Grid item >
                  <img src={item.logo} alt="logo" style={{height:"70px",width:"70px",cursor:"pointer",margin:"20px 0"}}/>
                </Grid>
             </div>
          )
      })
  };

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
        <Grid container>
          <Grid item sm={2}>
            <Grid
              container
              direction="column"
              justify="space-between"
              alignItems="center"
              spacing={2}
            >
              {_renderCinemaGroupICon(props.cinemaGroup)}
            </Grid>
          </Grid>
          <Grid item sm={10}>
            <HomePageShowTimeTabs/>
          </Grid>
        </Grid>
        </Paper>
      </div>
    );
}

const mapStateToProps = (state) => {
  return {
    cinemaGroup: state.homePageReducer.cinemaGroup,
    cinemaId: state.homePageReducer.cinemaId,
    showTime: state.homePageReducer.showTime,
  };
};
const mapDispatchToProps = (dispatch)=>{
  return{
    fetchShowTime: (id)=>{
      dispatch(actFetchShowTimeAPI(id));
  },
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(HomePageShowTime);
