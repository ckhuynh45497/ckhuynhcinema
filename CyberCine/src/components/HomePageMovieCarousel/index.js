import React from "react";
import { connect } from "react-redux";
import Carousel from "react-multi-carousel";

import "react-multi-carousel/lib/styles.css";
import { makeStyles } from "@material-ui/core/styles";
import TrailerPlayer from "../TrailerPlayer";
import { NavLink } from "react-router-dom";
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
  },
  tablet: {
    breakpoint: { max: 1024, min: 0 },
    items: 2,
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    "&:hover a": {
      opacity: "1",
    },
    position:"relative",
    paddingBottom:"20px"
  },
  button: {
    width: "60px",
    height: "60px",
    borderRadius: "50%",
    position: "absolute",
    left: "47%",
    top: "47%",
    background: "rgba(0,0,0,0.5)",
    opacity: "0",
    "&:hover": {
      background: "rgba(0,0,0,0.7)",
    },
    fontSize: "30px",
    lineHeight: "60px",
    textAlign: "center",
    "&:focus": {
      outline: "none",
      boxShadow: "none",
    },
    
  },
  btnBuy:{
    backgroundColor:"orange",
    color:"white",
    width:"60%",
    height:"50px",
    position:"absolute",
    top:"3px",
    left:"20%",
    opacity:"0",
    "&:hover": {
      background: "green",
      opacity:"1",
    },
  },
  carousel:{
    paddingTop:"30px",
    "& button":{
      outline:"none",
    },
  },
}));

function HomePageMovieCarousel(props) {
  const classes = useStyles();
  function _renderMovieItem(list) {
    return list.map((item, index) => {
      return (
        <div className={classes.root}>
          <div className={classes.root}>
            <img
              src={item.hinhAnh}
              alt="movie"
              style={{ width: "320px", height: "550px" }}
            />
           <TrailerPlayer link={item.trailer}/>
          </div>
          <div className={classes.root}>
          <p>{item.tenPhim}</p>
          <NavLink
              className={`${classes.btnBuy} btn btn-success`}
              variant="contained"
              color="primary"
              to={`/detail-movie/${item.maPhim}`}              
            >
              Chi Tiết
            </NavLink>
          </div>
        </div>
      );
    });
  }
  return (
    <div style={{paddingTop:"50px"}}>
      <h2>Đang chiếu</h2>
      <Carousel
        responsive={responsive}
        infinite={true}
        className="p-5"
        slidesToSlide={2}
        
      >
        {_renderMovieItem(props.listMovie)}
      </Carousel>
  
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    listMovie: state.homePageReducer.listMovie,
  };
};

export default connect(mapStateToProps)(HomePageMovieCarousel);
