import React, { Fragment, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import {
  actAddBookingSeat,
  actRemoveBookingSeat,
} from "../../containers/Home/MovieDetail/module/action";


const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: "40px",
    maxWidth: "40px",
    height: "40px",
    margin: "10px",
    outline: "none",
    backgroundColor: "#3e515d !important",
    color: "white",
    
  },
  booked: {
    minWidth: "40px",
    maxWidth: "40px",
    height: "40px",
    margin: "10px",
    outline: "none",   
    color: "white",
    
    backgroundColor: "red !important",
    cursor: "not-allowed !important",
  },
  vip: {
    backgroundColor: "yellow !important",
  },
  booking: {
    backgroundColor: "green !important",
  },
}));

function TicketBookingListItem(props) {
  const { root, booked, vip, booking } = useStyles();
  const [seatBooking, setseatBooking] = useState(false);
  let tenghe = null;
  if (props.seat) {
    tenghe = props.seat.tenGhe;
  }
  var btnClass = `${root}`;
  if (props.seat.loaiGhe === "Vip") {
    btnClass += ` ${vip}`;
  }
  if (seatBooking) {
    btnClass += ` ${booking}`;
  }
  if (props.seat.daDat) {
    btnClass += ` ${booked}`;
  }
  function _handleClickButton() {
    if (seatBooking) {
      props.removeBooking(props.seat.maGhe, props.seat.giaVe);
      setseatBooking(!seatBooking);
    } else {
      if (props.seatNumber >= 10) {
        alert("khong duoc dat qua 10 ve");
      } else {
        props.addBooking(props.seat.maGhe, props.seat.giaVe);
        setseatBooking(!seatBooking);
      }
    }
  }
  if(props.seat.daDat) return(
    <Fragment>
      <Button className={booked} disabled>
        {tenghe}
      </Button>
    </Fragment>
  )
  return (
    <Fragment>
      <Button className={btnClass} onClick={_handleClickButton}>
        {tenghe}
      </Button>
    </Fragment>
  );
}
const mapDispatchToProps = (dispatch) => {
  return {
    addBooking: (id, price) => {
      dispatch(actAddBookingSeat(id, price));
    },
    removeBooking: (id, price) => {
      dispatch(actRemoveBookingSeat(id, price));
    },
  };
};
const mapStateToProps = (state) => {
  return {
    seatNumber: state.ticketBookingReducer.seatNumber,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TicketBookingListItem);
