import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import HomePageShowTimeMovieItem from "../HomePageShowTimeMovieItem";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: "660px",
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    "& :focus": {
      outline: "none",
    },
  },
  text: {
    p: {
      fontSize: "14px",
    },
  },
}));

function HomePageShowTimeTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const _renderCinemaTab = (list) => {
    if (list) {
      return list.lstCumRap.map((item, index) => {
        return (
          <Tab
            label={
              <Grid
                container
                justify="space-between"
                alignItems="center"
                style={{ width: "100%", height: "100px", margin: "10px" }}
              >
                <Grid item xs={3}>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2018/09/bhd-star-vincom-3-2-15379527367766.jpg"
                    alt="cineImg"
                    style={{
                      height: "40px",
                      width: "40px",
                    }}
                  />
                </Grid>
                <Grid item xs={9}>
                  <p style={{ fontSize: "14px" }}>{item.tenCumRap}</p>
                  <p style={{ fontSize: "12px" }}>{`${item.diaChi.slice(
                    0,
                    20
                  )}...`}</p>
                </Grid>
              </Grid>
            }
            {...a11yProps(index)}
          />
        );
      });
    }
    return;
  };
  const _renderCinemaMovie = (list) => {
    if (list) {
      return list.lstCumRap.map((item, indexl) => {
        return (
          <TabPanel value={value} index={indexl}>
            <HomePageShowTimeMovieItem movies={item.danhSachPhim} />
          </TabPanel>
        );
      });
    }
    return <p>Hiện không có lịch chiếu</p>;
  };

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        {_renderCinemaTab(props.showTime[0])}
      </Tabs>
      {_renderCinemaMovie(props.showTime[0])}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    showTime: state.homePageReducer.showTime,
  };
};

export default connect(mapStateToProps)(HomePageShowTimeTabs);
