import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    width: "60px",
    height: "60px",
    borderRadius: "50%",
    position: "absolute",
    left: "47%",
    top: "47%",
    background: "rgba(0,0,0,0.5)",
    opacity: "0",
    "&:hover": {
      background: "rgba(0,0,0,0.7)",
    },
    fontSize: "30px",
    lineHeight: "60px",
    textAlign: "center",
    "&:focus": {
      outline: "none",
      boxShadow: "none",
    },
  },
}));

export default function TrailerPlayer(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <button type="button" onClick={handleOpen} className={classes.button}>
      <i class="fa fa-play"></i>
      </button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <iframe
          width="1280"
          height="720"
          src={props.link}
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
          title="trailer"
        ></iframe>
      </Modal>
    </div>
  );
}
