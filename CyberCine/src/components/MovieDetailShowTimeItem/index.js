import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import MovieDetailCinemaShowTimeItem from "../MovieDetailCinemaShowTimeItem";

function MovieDetailShowtimeItem(props) {
  function _renderShowtimeItem() {
    return props.cinemaList.map((item, index) => {
      return (
        <Paper variant="outlined" style={{marginBottom:"30px",padding:"20px"}}>
          <Grid
            container
            spacing={2}
            direction="row"
            justify="space-around"
            alignItems="center"
          >
            <Grid item xs={3}>
              <img
                src="https://s3img.vcdn.vn/123phim/2018/09/cgv-crescent-mall-15380174094679.jpg"
                alt="cinema"
                style={{ width: "120px", height: "120px" }}
              />
            </Grid>
            <Grid item xs={9} style={{ textAlign: "left" }}>
              <p>{item.tenCumRap}</p>
              <p>dia chi</p>
            </Grid>
            <Grid item xs={12} style={{ textAlign: "left", margin: "20px 0" }}>
              <h2>2D Digital</h2>
              <MovieDetailCinemaShowTimeItem
                showtimeList={item.lichChieuPhim}
              />
            </Grid>
          </Grid>
        </Paper>
      );
    });
  }

  return <>{_renderShowtimeItem()}</>;
}

export default MovieDetailShowtimeItem;
