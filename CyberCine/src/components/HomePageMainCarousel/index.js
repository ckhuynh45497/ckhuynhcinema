import React, { Fragment } from "react";
import { connect } from "react-redux";
// import Carousel from "react-material-ui-carousel";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import Carousel from "react-bootstrap/Carousel";
import TrailerPlayer from "../TrailerPlayer";

const useStyle = makeStyles((theme) => ({
  root: {
    "&:hover button": {
      opacity: "1",
    },
    position:"relative"
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  trailer: {    
    position: "absolute",
    left: "47%",
    top: "47%",    
  },
  image: {
    width: "100%",
    height: "800px",
  },
  btnClose: {
    position: "absolute",
    top: "0",
    right: "0",
    background: "transparent",
    color: "black",
    width: "40px",
    height: "40px",
    borderRadius: "50%",
    lineHeight: "40px",
    fontSize: "20px",
    border: "5px solid red",
    transform: "translate(20px,20px)",
  },
  dialogTitle: {
    position: "relative",
  },
  dialog: {
    background: "transparent",
    overflow: "hiden",
  },
}));

function HomePageMainCarousel(props) {
  const classes = useStyle();
 
  function _renderMovieCarousel(list) {
    return list.map((item, index) => {
      return (
        <Carousel.Item>
          <Paper className={classes.root}>
            <img
              src={item.hinhAnh}
              alt="movieCarousel"
              className={classes.image}
            />
            
              <TrailerPlayer link={item.trailer} />
            
          </Paper>
        </Carousel.Item>
      );
    });
  }
  return (
    <Fragment>
      <Carousel indicators={false} fade={true}>
        {_renderMovieCarousel(props.mainCarousel)}
      </Carousel>
    </Fragment>
  );
}
const mapStateToProps = (state) => {
  return {
    mainCarousel: state.homePageReducer.mainCarousel,
  };
};

export default connect(mapStateToProps)(HomePageMainCarousel);
