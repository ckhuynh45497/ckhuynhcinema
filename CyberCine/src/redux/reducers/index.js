import {combineReducers} from "redux";
import homePageReducer from "./../../containers/Home/HomePage/module/reducer"
import movieDetailReducer from "./../../containers/Home/MovieDetail/module/reducer"
import ticketBookingReducer from "./../../containers/Home/TicketBooking/module/reducer"
import authReducer from "./../../containers/Admin/Auth/modules/reducer"
import addUserReducer from "./../../containers/Admin/AddUser/modules/reducer"

const rootReducer = combineReducers({
    homePageReducer,
    movieDetailReducer,
    ticketBookingReducer,
    authReducer,
    addUserReducer,
});

export default rootReducer;